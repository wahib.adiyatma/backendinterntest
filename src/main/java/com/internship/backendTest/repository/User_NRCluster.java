package com.internship.backendTest;

import com.internship.backendTest.model.User_NRCluster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface User_NRCluster extends JpaRepository<User_NRCluster, String> {
    User_NRCluster findByKodeMatkul(String user_nrcluster_id);
}