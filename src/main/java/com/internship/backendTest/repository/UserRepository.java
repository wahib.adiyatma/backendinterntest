package com.internship.backendTest;

import com.internship.backendTest.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, String> {
    User findByKodeMatkul(String user_id);
}