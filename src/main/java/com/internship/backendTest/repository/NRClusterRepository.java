package com.internship.backendTest;

import com.internship.backendTest.model.NR_Cluster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NRClusterRepository extends JpaRepository<NR_Cluster, String> {
    NR_Cluster findByKodeMatkul(String nrcluster_id);
}