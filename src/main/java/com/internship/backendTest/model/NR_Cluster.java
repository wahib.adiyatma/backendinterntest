package com.internship.backendTest;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.*;
import java.util.List;
import java.sql.Timestamp;

@Entity
@Table(name = "NRCluster")
@Data
@NoArgsConstructor
public class User {
    @Id
    @Column(name = "nrcluster_id", updatable = false, nullable = false, length = 10)
    private String nrcluster_id;

    @Column(name = "address", unique = true)
    private String address;

    @Column(name = "city", unique = true)
    private String city;

    @Column(name = "lattitude")
    private String lattitude;

    @Column(name = "longitude")
    private String longitude;

    // Realtionship one to many with user_NRCluster
    @OneToMany(mappedBy = "user_nrcluster", cascade = CascadeType.ALL)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private List<User_NRCluster> user_NRCluster;

    public User(String nrcluster_id, String address, String city, String lattitude, String longitude) {
        this.nrcluster_id = nrcluster_id;
        this.address = address;
        this.city = city;
        this.lattitude = lattitude;
        this.longitude = longitude;
    }
}
