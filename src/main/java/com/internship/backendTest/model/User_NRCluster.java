package com.internship.backendTest;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.*;
import java.util.List;
import java.sql.Timestamp;

@Entity
@Table(name = "user_nrcluster")
@Data
@NoArgsConstructor
public class User {
    @Id
    @Column(name = "user_nrcluster_id", updatable = false, nullable = false, length = 10)
    private String user_nrcluster_id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "kodeMatkul")
    @JsonIdentityReference(alwaysAsId = true)
    @JoinColumn(name = "user_id")
    private User id_user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "kodeMatkul")
    @JsonIdentityReference(alwaysAsId = true)
    @JoinColumn(name = "nrcluster_id")
    private NR_Cluster nrcluster_id;

    @Column(name = "NR_Cluster")
    private String NR_Cluster;

    public User(String user_nrcluster_id, User id_user, NR_Cluster nrcluster_id, String NR_Cluster) {
        this.user_nrcluster_id = user_nrcluster_id;
        this.id_user = id_user;
        this.nrcluster_id = nrcluster_id;
        this.NR_Cluster = NR_Cluster;
    }
}
