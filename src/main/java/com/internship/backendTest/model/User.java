package com.internship.backendTest;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.*;
import java.util.List;
import java.sql.Timestamp;

@Entity
@Table(name = "user")
@Data
@NoArgsConstructor
public class User {
    @Id
    @Column(name = "user_id", updatable = false, nullable = false, length = 10)
    private String user_id;

    @Column(name = "name", unique = true)
    private String name;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name = "address")
    private String address;

    @Column(name = "email")
    private String email;

    @Column(name = "phone_number")
    private String phone_number;

    @Column(name = "date_of_birth", nullable=false)
    private TimeStamp date_of_birth;

    // Realtionship one to many with user_NRCluster
    @OneToMany(mappedBy = "user_nrcluster", cascade = CascadeType.ALL)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private List<User_NRCluster> user_NRCluster;


    public User(String user_id, String name, String address, String email, String phone_number, TimeStamp date_of_birth) {
        this.user_id = user_id;
        this.name = name;
        this.address = address;
        this.email = email;
        this.phone_number = phone_number;
        this.date_of_birth = date_of_birth;
    }
}
